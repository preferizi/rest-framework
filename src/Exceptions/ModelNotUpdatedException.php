<?php
namespace RestInABox\Framework\Exceptions;

use Illuminate\Contracts\Support\MessageProvider;

/**
 * Class ModelNotUpdatedException
 * @package RestInABox\Framework\Exceptions
 */
class ModelNotUpdatedException extends ModelException implements MessageProvider
{
}
