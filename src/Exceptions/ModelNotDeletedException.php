<?php
namespace RestInABox\Framework\Exceptions;

use Illuminate\Contracts\Support\MessageProvider;

/**
 * Class ModelNotDeletedException
 * @package RestInABox\Framework\Exceptions
 */
class ModelNotDeletedException extends ModelException implements MessageProvider
{
}
