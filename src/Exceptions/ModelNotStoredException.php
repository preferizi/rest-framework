<?php
namespace RestInABox\Framework\Exceptions;

use Illuminate\Contracts\Support\MessageProvider;

/**
 * Class ModelNotStoredException
 * @package RestInABox\Framework\Exceptions
 */
class ModelNotStoredException extends ModelException implements MessageProvider
{
}
