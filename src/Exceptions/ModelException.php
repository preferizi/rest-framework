<?php
namespace RestInABox\Framework\Exceptions;

use Illuminate\Contracts\Support\MessageProvider;
use Illuminate\Support\MessageBag;

/**
 * Class ModelException
 * @package RestInABox\Framework\Exceptions
 */
abstract class ModelException extends \RuntimeException implements MessageProvider
{
    /**
     * Name of the affected Eloquent model.
     *
     * @var string
     */
    protected $model;

    /**
     * @var MessageBag
     */
    protected $errors;

    /**
     * @param string $model
     * @param MessageBag|array|null $errors
     * @param string $message
     */
    public function __construct($model, $errors = null, $message = "")
    {
        $this->model = $model;
        $this->errors = is_array($errors) ? new MessageBag($errors) : $errors;

        parent::__construct($message);
    }

    /**
     * Get the affected Eloquent model.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return MessageBag
     */
    public function getMessageBag()
    {
        return $this->errors;
    }
}
