<?php
namespace RestInABox\Framework\Serializer;

use Illuminate\Support\Str;
use League\Fractal\Serializer\ArraySerializer;

/**
 * Class NaturalSerializer
 * This serializer does not change the array output at all.
 * @package RestInABox\Framework\Serializer
 */
class NaturalSerializer extends ArraySerializer
{
    /**
     * Serialize an item.
     *
     * @param string $resourceKey
     * @param array $data
     *
     * @return array
     */
    public function item($resourceKey, array $data)
    {
        if (false === $resourceKey) {
            return $data;
        }
        return array(Str::singular($resourceKey) ?: 'data' => $data);
    }

    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        if (false === $resourceKey || is_null($resourceKey)) {
            return $data;
        }
        return array(Str::plural($resourceKey) ?: 'data' => $data);
    }
}
