<?php
namespace RestInABox\Framework\Transformer\Adapter;

use Dingo\Api\Contract\Transformer\Adapter;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\Presentable;
use RestInABox\Framework\Repository\Contracts\MetaDataPresenterInterface;

/**
 * Class PresentableTransformer
 * @package RestInABox\Framework\Transformer\Adapter
 */
class PresentableTransformer implements Adapter
{
    /**
     * Transform a response with a transformer.
     *
     * @param mixed $response
     * @param object $transformer
     * @param Binding $binding
     * @param Request $request
     *
     * @return array
     */
    public function transform($response, $transformer, Binding $binding, Request $request)
    {
        // Only transform a presentable
        $presentable = $response;
        if ($presentable instanceof LengthAwarePaginator) {
            $presentable = collect($presentable->items());
        }
        if ($presentable instanceof Collection) {
            $presentable = $presentable->first();
        }
        if (!$presentable instanceof Presentable) {
            return $response;
        }

        // Run presenter if we got a presentable object
        $presenter = $presentable->presenter();
        if ($presenter instanceof MetaDataPresenterInterface) {
            $presenter->setMeta($binding->getMeta());
        }
        return $presenter->present($response);
    }
}
