<?php
namespace RestInABox\Framework\Providers;

use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager as FractalManager;
use Illuminate\Validation\Factory;

/**
 * Class RestApiServiceProvider
 * @package RestInABox\Framework\Providers
 */
class RestApiServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Setup config
        $this->setupConfig();

        // Register Framework bindings
        $this->registerFractal();

        // Register the Repository EventServiceProvider
        $this->app->register('Prettus\Repository\Providers\EventServiceProvider');

        // Register Dingo API Services
        $this->app->register('Dingo\Api\Provider\LumenServiceProvider');

        // Register validator bindings
        $this->registerValidatorBindings();

        // Overwrite configurations with ours again
        $this->setupConfig();
    }

    /**
     * Setup the configuration.
     *
     * @return void
     */
    protected function setupConfig()
    {
        $this->mergeConfigFrom(realpath(__DIR__ . '/../../config/api.php'), 'api');
        $this->mergeConfigFrom(realpath(__DIR__ . '/../../config/repository.php'), 'repository');
    }

    /**
     * Register the Fractal serializer from the configuration.
     *
     * @return void
     */
    protected function registerFractal()
    {
        $this->app->singleton(FractalManager::class, function ($app) {
            $fractal = new FractalManager;
            $fractal->setSerializer($this->prepareConfigValue($app['config']['api.serializer']));
            return $fractal;
        });
    }

    /**
     * Register validator bindings for the application.
     *
     * @return void
     */
    protected function registerValidatorBindings()
    {
        $this->app->bind(Factory::class, function () {
            return $this->app->make('validator');
        });
    }

    /**
     * Prepare an instantiable configuration instance.
     *
     * @param mixed $instance
     *
     * @return object
     */
    protected function prepareConfigValue($instance)
    {
        if (is_string($instance)) {
            return $this->app->make($instance);
        }

        return $instance;
    }
}
