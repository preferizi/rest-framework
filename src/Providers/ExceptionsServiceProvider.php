<?php
namespace RestInABox\Framework\Providers;

use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\ServiceProvider;
use RestInABox\Framework\Exceptions\ModelConflictException;
use RestInABox\Framework\Exceptions\ModelNotDeletedException;
use RestInABox\Framework\Exceptions\ModelNotStoredException;
use RestInABox\Framework\Exceptions\ModelNotUpdatedException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Dingo\Api\Exception\Handler as ExceptionHandler;

/**
 * Class ExceptionsServiceProvider
 * @package RestInABox\Framework\Providers
 */
class ExceptionsServiceProvider extends ServiceProvider
{
    /**
     * The exception handler to register custom responses
     * @var ExceptionHandler
     */
    protected $handler;

    /**
     * ExceptionsServiceProvider constructor.
     * @param \Illuminate\Contracts\Foundation\Application $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->handler = $this->app['api.exception'];
        $this->registerModelExceptionsResponses();
        $this->registerExceptionsResponses();
    }

    /**
     * Extend the ExceptionHandler with default ModelExceptions
     */
    protected function registerModelExceptionsResponses()
    {
        // ModelNotFoundException
        $this->handler->register(function (ModelNotFoundException $exception) {
            throw new NotFoundHttpException(sprintf("%s not found.", class_basename($exception->getModel())));
        });
        // ModelConflictException
        $this->handler->register(function (ModelConflictException $exception) {
            throw new ConflictHttpException(sprintf('%s has been updated prior to the request.', class_basename($exception->getModel())));
        });
        // ModelNotUpdatedException
        $this->handler->register(function (ModelNotUpdatedException $exception) {
            throw new UpdateResourceFailedException(sprintf('Could not update %s.', class_basename($exception->getModel())), $exception->getMessageBag());
        });
        // ModelNotDeletedException
        $this->handler->register(function (ModelNotDeletedException $exception) {
            throw new DeleteResourceFailedException(sprintf('Could not remove %s.', class_basename($exception->getModel())), $exception->getMessageBag());
        });
        // ModelNotDeletedException
        $this->handler->register(function (ModelNotStoredException $exception) {
            throw new StoreResourceFailedException(sprintf('Could not store %s.', class_basename($exception->getModel())), $exception->getMessageBag());
        });
    }

    /**
     * Extend the ExceptionHandler with custom exception responses.
     */
    protected function registerExceptionsResponses()
    {
    }
}
