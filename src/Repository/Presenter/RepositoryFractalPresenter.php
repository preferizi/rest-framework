<?php
namespace RestInABox\Framework\Repository\Presenter;

use Dingo\Api\Transformer\Factory as TransformerFactory;
use League\Fractal\TransformerAbstract;
use RestInABox\Framework\Repository\Contracts\MetaDataPresenterInterface;

/**
 * Class RepositoryFractalPresenter
 * @package RestInABox\Framework\Repository\Presenter
 */
class RepositoryFractalPresenter extends MetaDataPresenter implements MetaDataPresenterInterface
{
    /**
     * @var TransformerAbstract
     */
    protected $transformer;

    /**
     * @var TransformerFactory
     */
    protected $transformerFactory;

    /**
     * MetaDataPresenter constructor.
     * @param TransformerFactory $transformerFactory
     * @throws \Exception
     */
    public function __construct(TransformerFactory $transformerFactory)
    {
        parent::__construct();
        $this->transformerFactory = $transformerFactory;
    }

    /**
     * Set model transformer and register binding with TransformerFactory.
     * @param TransformerAbstract $transformer
     * @param object|string $model
     */
    public function setTransformer(TransformerAbstract $transformer, $model)
    {
        // set transformer
        $this->transformer = $transformer;

        // determine class name of model
        $class = is_object($model) ? get_class($model) : $model;

        // register binding with transformer factory
        $this->transformerFactory->register($class, $transformer);
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return $this->transformer;
    }
}
