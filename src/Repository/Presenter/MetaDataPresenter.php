<?php
namespace RestInABox\Framework\Repository\Presenter;

use Illuminate\Pagination\AbstractPaginator;
use League\Fractal\Scope;
use Prettus\Repository\Presenter\FractalPresenter;
use Exception;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use RestInABox\Framework\Repository\Contracts\MetaDataPresenterInterface;

/**
 * Class MetaDataPresenter
 * @package RestInABox\Framework\Repository\Presenter
 */
abstract class MetaDataPresenter extends FractalPresenter implements MetaDataPresenterInterface
{
    /**
     * @var array
     */
    protected $meta;

    /**
     * Set meta data to the presenter.
     * @param array $data
     * @return $this
     */
    public function setMeta(array $data)
    {
        $this->meta = $data;
        return $this;
    }

    /**
     * Return the stored meta data.
     * @return array
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Prepare data to present
     *
     * @param $data
     *
     * @return Scope
     * @throws Exception
     */
    public function present($data)
    {
        if (!class_exists('League\Fractal\Manager')) {
            throw new Exception(trans('repository::packages.league_fractal_required'));
        }

        if ($data instanceof EloquentCollection) {
            $this->resource = $this->transformCollection($data);
        } elseif ($data instanceof AbstractPaginator) {
            $this->resource = $this->transformPaginator($data);
        } else {
            $this->resource = $this->transformItem($data);
        }

        // add meta data
        foreach ($this->getMeta() as $key => $value) {
            $this->resource->setMetaValue($key, $value);
        }

        return $this->fractal->createData($this->resource)->toArray();
    }
}
