<?php
namespace RestInABox\Framework\Repository\Contracts;

/**
 * Interface MetaDataPresenterInterface
 * @package RestInABox\Framework\Repository\Contracts
 */
interface MetaDataPresenterInterface
{
    /**
     * Set meta data to the presenter.
     * @param array $data
     * @return $this
     */
    public function setMeta(array $data);

    /**
     * Return the stored meta data.
     * @return array
     */
    public function getMeta();
}
