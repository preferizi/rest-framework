<?php
namespace RestInABox\Framework\Repository\Contracts;

use League\Fractal\TransformerAbstract;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TransformerAwareRepositoryInterface
 * @package RestInABox\Framework\Repository\Contracts
 */
interface TransformerAwareRepositoryInterface extends RepositoryInterface
{
    /**
     * Specify Transformer class name
     *
     * @return TransformerAbstract
     */
    public function transformer();
}
