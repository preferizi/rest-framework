<?php
namespace RestInABox\Framework\Repository\Eloquent;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Contracts\PresenterInterface;
use Prettus\Repository\Eloquent\BaseRepository as Repository;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use RestInABox\Framework\Exceptions\ModelConflictException;
use RestInABox\Framework\Exceptions\ModelNotStoredException;
use RestInABox\Framework\Exceptions\ModelNotUpdatedException;
use RestInABox\Framework\Repository\Contracts\ConflictAwareRepositoryInterface;
use RestInABox\Framework\Repository\Contracts\TransformerAwareRepositoryInterface;
use RestInABox\Framework\Repository\Contracts\ValidationAwareRepositoryInterface;
use RestInABox\Framework\Repository\Presenter\RepositoryFractalPresenter;

/**
 * Class BaseRepository
 * @package RestInABox\Framework\Repository\Eloquent
 */
abstract class BaseRepository extends Repository implements
    ValidationAwareRepositoryInterface,
    TransformerAwareRepositoryInterface,
    ConflictAwareRepositoryInterface
{
    /**
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * @var TransformerAbstract
     */
    protected $transformer;

    /**
     * Field used for the conflict check
     * @var string
     */
    protected $updated_at = 'updated_at';

    /**
     * Specify Transformer class name
     *
     * @return TransformerAbstract
     */
    abstract public function transformer();

    /**
     * @param null $presenter
     *
     * @return PresenterInterface
     * @throws RepositoryException
     */
    public function makePresenter($presenter = null)
    {
        $presenter = !is_null($presenter) ? $presenter : $this->presenter();

        // create default presenter if not set
        if (is_null($presenter)) {
            $this->presenter = $this->app->make(RepositoryFractalPresenter::class);
            $this->presenter->setTransformer($this->transformer(), $this->model());
            return $this->presenter;
        }

        return parent::makePresenter($presenter);
    }

    /**
     * @inheritDoc
     */
    public function noConflict($model, array $attributes)
    {
        // Only if repository has no conflict enabled
        if (!$this->updated_at) {
            return;
        }

        // Find time of last update from attributes
        $updated_at = new Carbon(array_get($attributes, $this->updated_at));

        // Check for conflict
        if (isset($model->updated_at) && $updated_at->ne($model->updated_at)) {
            throw new ModelConflictException($model, $this->updated_at);
        }
    }

    /**
     * @inheritDoc
     */
    public function create(array $attributes)
    {
        try {
            $this->validate($attributes, ValidatorInterface::RULE_CREATE);
        } catch (ValidatorException $exception) {
            throw new ModelNotStoredException($this->model->newInstance(), $exception->getMessageBag());
        }
        $model = $this->model->newInstance($attributes);
        $model->save();
        $this->resetModel();

        event(new RepositoryEntityCreated($this, $model));

        return $this->parserResult($model);
    }

    /**
     * @inheritDoc
     */
    public function update(array $attributes, $id)
    {
        $this->applyScope();
        try {
            $this->validate($attributes, ValidatorInterface::RULE_UPDATE, $id);
        } catch (ValidatorException $exception) {
            throw new ModelNotUpdatedException($this->model->newInstance(), $exception->getMessageBag());
        }

        $temporarySkipPresenter = $this->skipPresenter;

        $this->skipPresenter(true);

        $model = $this->model->findOrFail($id);

        $this->noConflict($model, $attributes);

        $model->fill($attributes);
        $model->save();

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
    }

    /**
     * @inheritDoc
     */
    public function updateOrCreate(array $attributes, array $values = [])
    {
        $this->applyScope();

        try {
            $this->validate($attributes, ValidatorInterface::RULE_UPDATE);
        } catch (ValidatorException $exception) {
            throw new ModelNotUpdatedException($this->model->newInstance(), $exception->getMessageBag());
        }

        $temporarySkipPresenter = $this->skipPresenter;

        $this->skipPresenter(true);

        $model = $this->model->updateOrCreate($attributes, $values);

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
    }


    /**
     * Validate input data for an entity.
     * @param array $attributes
     * @param string $rules
     * @param null $id
     * @return bool
     * @throws ValidatorException
     */
    public function validate(array $attributes, $rules = ValidatorInterface::RULE_CREATE, $id = null)
    {
        if (!is_null($this->validator)) {
            $this->validator->with($attributes)->setId($id)->passesOrFail($rules);
        }
    }
}
