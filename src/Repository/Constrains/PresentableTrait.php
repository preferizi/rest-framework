<?php
namespace RestInABox\Framework\Repository\Constrains;

use Illuminate\Contracts\Support\Arrayable;
use Prettus\Repository\Contracts\PresenterInterface;

/**
 * Class PresentableTrait
 * @package RestInABox\Framework\Repository\Constrains
 */
trait PresentableTrait
{
    /**
     * @var PresenterInterface
     */
    protected $presenter = null;

    /**
     * @param \Prettus\Repository\Contracts\PresenterInterface $presenter
     *
     * @return $this
     */
    public function setPresenter(PresenterInterface $presenter)
    {
        $this->presenter = $presenter;

        return $this;
    }

    /**
     * @param $data
     * @return mixed|null
     */
    public function present($data)
    {
        if ($this->hasPresenter()) {
            return $this->presenter->present($this);
        }

        return ($data instanceof Arrayable ? $data->toArray() : $data);
    }

    /**
     * @return bool
     */
    protected function hasPresenter()
    {
        return isset($this->presenter) && $this->presenter instanceof PresenterInterface;
    }

    /**
     * @return $this|mixed
     */
    public function presenter()
    {
        if ($this->hasPresenter()) {
            return $this->presenter;
        }

        return $this;
    }
}
