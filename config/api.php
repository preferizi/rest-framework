<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Response Transformer
    |--------------------------------------------------------------------------
    |
    | Responses can be transformed so that they are easier to format. By
    | default a Fractal transformer will be used to transform any
    | responses prior to formatting. You can easily replace
    | this with your own transformer.
    |
    */

    'transformer' => env('API_TRANSFORMER', \RestInABox\Framework\Transformer\Adapter\PresentableTransformer::class),

    /*
    |--------------------------------------------------------------------------
    | Response Serializer
    |--------------------------------------------------------------------------
    |
    | A Serializer structures your Transformed data in certain ways. There are
    | many output structures for APIs, two popular ones being HAL and JSON-API.
    | Twitter and Facebook output data differently to each other, and Google
    | does it differently too. Most of the differences between these serializers
    | are how data is namespaced.
    |
    */

    'serializer' => env('API_SERIALIZER', \League\Fractal\Serializer\DataArraySerializer::class),
];
