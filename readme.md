# REST-in-a-box (Framework)

> **Note:** This repository contains the framework package for REST-in-a-box. If you want to build a REST API using REST-in-a-box, visit the main [REST-in-a-box repository](https://bitbucket.org/preferizi/rest-in-a-box).

## REST-in-a-box
REST-in-a-box is a comprehensive package for building fast and flexible REST APIs in PHP. It is based on the popular Laravel Lumen micro-framework and combines a range of Lumen packages into a reliable and harmonized solution that should work for a majority of REST API projects. Therefore REST-in-a-box will enforce certain development patterns, which we strongly believe to be the best way to build maintainable REST APIs

## Documentation

Documentation for REST-in-a-box can be found on the [REST-in-a-box repository](https://bitbucket.org/preferizi/rest-in-a-box).

## License

REST-in-a-box is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
